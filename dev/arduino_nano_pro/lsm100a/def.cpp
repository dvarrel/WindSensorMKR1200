#include "HardwareSerial.h"
#include "Arduino.h"
#include "def.h"


void delay_ms(uint32_t ms){
  uint32_t d = ms / clock_divisor;
  if (d==0) {
    delayMicroseconds(ms * 1000 / clock_divisor);
  }
  else{
    delay(d);
  }
}

void SigfoxSend(const char* ATcode){
  SerialSigfox.print(ATcode);
  SerialSigfox.print("\r");
  SerialSigfox.flush();
}

char* SigfoxReceive(uint16_t timeOut_ms){
  static char str[128];
  memset(str, 0, sizeof(str));
  uint16_t waiting_ms = 0;
  while (waiting_ms++ <= timeOut_ms){
    if (SerialSigfox.available()>0) break;
    delay_ms(1);
  } 
  uint8_t i = 0;
  while (SerialSigfox.available()){
    char c = SerialSigfox.read();
		if (c != 0x0A && c != 0x0D && i<sizeof(str)){//0x0A Line feed | 0x0D Carriage return
			str[i] = c;
      i++;
    }
    delay_ms(1);
  }
  str[i]='\0';
  return str;
}

bool SigfoxConfigModule(){
  SigfoxSend("AT+MODE=?");
  uint16_t mode;
  sscanf(SigfoxReceive(), "%u", &mode);
  if (mode != 0){
    SigfoxSend("AT+MODE=0"); // 0:Sigfox 1:Lora
    //module reset on changing mode
    delay_ms(1);
    while (SerialSigfox.available()){
      SerialSigfox.read();
      delay_ms(1);
    }
    SigfoxSend("AT+MODE=?");
    sscanf(SigfoxReceive(), "%u", &mode);
    SigfoxSend("AT+VL=0"); // low Verbose Level,
    SigfoxReceive();
  }
  return mode == 0;
}

void SigfoxInfos(){
  #define n 7
  const char* command[n] = {"AT",  "AT+BAT=?", "AT+MODE=?", "AT+VER=?", "AT$ID", "AT$PAC", "ATS302=?"};
  const char* name[n] = {"Test",  "Battery level", "Mode Sigfox(0) Lora(1)", "fw version", "ID", "PAC", "Radio output power"};
  char s[128];
  for (uint8_t i = 0; i < n; i++) {
    strcpy (s,command[i]);
    SigfoxSend(s);
    Serial.print(name[i]);
    Serial.print(" ");
    Serial.println(SigfoxReceive());
  }
}

void trimChar(char* s){
  char ts[128];
  strcpy (ts,s);
  uint8_t i=0;
  while(ts[i]==' ')i++; 
  uint8_t j=0;
  while(ts[i]!='\0'){
    s[j]=ts[i];
    i++;
    j++;
  }
  s[j]='\0';
}



