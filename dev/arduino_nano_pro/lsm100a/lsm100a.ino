#include <Arduino.h>
#include "def.h"

SoftwareSerial SerialSigfox(rxPin, txPin); // RX, TX
uint8_t clock_divisor = 1;

void setup()
{
  pinMode(LED, OUTPUT);
  pinMode(pinRST, OUTPUT);
  digitalWrite(pinRST, HIGH);

  Serial.begin(9600);
  SerialSigfox.begin(9600); //module Sigfox 9600bauds
  delay_ms(1000);
  Serial.println(SigfoxReceive(1000));
  Serial.print("LSM100A_config ");
  Serial.println(SigfoxConfigModule());
  SigfoxInfos();

  digitalWrite(LED, HIGH);
  //char frame[] = "AT$SF=414243444546";
  //SigfoxSend(frame);Serial.print(frame);
  //Serial.println(SigfoxReceive(10000));
  digitalWrite(LED, LOW);
}

void loop() 
{
  if (Serial.available()){
    static char str[128];
    memset(str, 0, sizeof(str));
    uint8_t i = 0;
    while (Serial.available()>0){
      char c = Serial.read();
      if (c != 0x0A && c != 0x0D) {//0x0A Line feed | 0x0D Carriage return
        str[i] = c;
        i++;
      }
      delay_ms(1);
    }
    trimChar(str);
    Serial.println(str);
    SigfoxSend(str);
    Serial.print(SigfoxReceive());    
  }
}