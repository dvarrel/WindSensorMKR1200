#ifndef DEF_H_
#define DEF_H_
#include <Arduino.h>
#include <SoftwareSerial.h>

#define rxPin 6
#define txPin 4
extern SoftwareSerial SerialSigfox;
#define pinRST 7
extern uint8_t clock_divisor;
#define LED LED_BUILTIN

void delay_ms(uint32_t ms);
bool SigfoxConfigModule();
void SigfoxSend(const char* ATcode);
char* SigfoxReceive(uint16_t timeOut_ms=200);
void SigfoxInfos();
void trimChar(char* s);

#endif