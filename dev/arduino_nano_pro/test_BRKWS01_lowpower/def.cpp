#include "Arduino.h"
#include "def.h"

uint16_t ReadTCNT1()
{
  noInterrupts();
  uint16_t c = TCNT1; //16bits counter on pin 5
  TCNT1 = 0;
  interrupts();
  return c;
}

void setClock(uint8_t divisor){
  noInterrupts();
  CLKPR = 0x80; // enable clock prescale change
  CLKPR = divisor;    // no prescale
  interrupts();
  clock_divisor = pow(2,divisor);
}

void delay_ms(uint32_t ms){
  uint32_t d = ms / clock_divisor;
  if (d==0) {
    delayMicroseconds(ms * 1000 / clock_divisor);
  }
  else{
    delay(d);
  }
}

void SigfoxSend(String ATcode){
  ATcode +="\r";
  Sigfox.print(ATcode);
  Sigfox.flush();
}

String SigfoxReceive(uint16_t timeOut_ms=100){
  String data="";
  char c;
  uint16_t count = 0;
  while (count < timeOut_ms){
    count++;
    if (Sigfox.available()>0) break;
    delay_ms(1);
  } 
  while (Sigfox.available()){
    c = Sigfox.read();
		if (c != 0x0A && c != 0x0D){//0x0A Line feed | 0x0D Carriage return
			data += c;
    } 
    delay_ms(1);
  }
  return data;
}

void infoSigfox(){
  digitalWrite(pinWakeUp, 0);
  delay_ms(100);
  digitalWrite(pinWakeUp, 1);
  SigfoxSend("AT");Serial.print("AT ");//Communication test
  Serial.println(SigfoxReceive());
  SigfoxSend("AT$I=10");Serial.print("AT$I=10 ");//Get Module ID
  Serial.println(SigfoxReceive());
  SigfoxSend("AT$I=11");Serial.print("AT$I=11 ");//Get PAC code
  Serial.println(SigfoxReceive());
  SigfoxSend("AT$V?");Serial.print("AT$V? ");//get temperature
  Serial.println(SigfoxReceive());
  SigfoxSend("AT$T?");Serial.print("AT$T? ");//get voltage
  Serial.println(SigfoxReceive());
  SigfoxSend("AT$P=2");Serial.print("AT$P=2 ");//deep sleep
  Serial.println(SigfoxReceive());Serial.flush();
}

void Sigfox_send_message(){
  digitalWrite(pinWakeUp, 0);
  delay_ms(100);
  digitalWrite(pinWakeUp, 1);
  SigfoxSend("AT$SF=123456789ABC");Serial.print("AT$SF=123456789ABC ");
  Serial.println(SigfoxReceive(10000));

  SigfoxSend("AT$V?");Serial.print("AT$V? ");
  Serial.println(SigfoxReceive());

  SigfoxSend("AT$P=2");Serial.print("AT$P=2 ");
  Serial.println(SigfoxReceive());

  delay_ms(50);
}



