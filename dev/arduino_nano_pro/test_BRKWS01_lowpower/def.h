#ifndef DEF_H_
#define DEF_H_
#include <Arduino.h>
#include <SoftwareSerial.h>

enum clock_divisor_t
{
	CLOCK_DIVISOR_1,CLOCK_DIVISOR_2,CLOCK_DIVISOR_4,CLOCK_DIVISOR_8,CLOCK_DIVISOR_16,
  CLOCK_DIVISOR_32,CLOCK_DIVISOR_64,CLOCK_DIVISOR_128,CLOCK_DIVISOR_256
};
#define rxPin 6
#define txPin 4
extern SoftwareSerial Sigfox;
#define pinWakeUp 7  //Sigfox GPIO9
extern uint8_t clock_divisor;
void setClock(uint8_t divisor);
#define slowClock() setClock(CLOCK_DIVISOR_256)
#define fastClock() setClock(CLOCK_DIVISOR_1)
#define LED LED_BUILTIN
#define N 10
#define pinBat A2
//voltage divisor R1=180k R2=11k Max Battery Voltage = 7.9V
#define Vdiv (179.4+11.26)/11.26
#define ErrorUref 1.0
#define Vinternal 1.1
#define Uref Vinternal * ErrorUref


#define pinGirb0 A0
#define pinGirb1 9
#define pinGirb2 8
#define pinGirAlim A3

uint16_t ReadTCNT1(); 
void delay_ms(uint32_t ms);
void SigfoxSend(String ATcode);
String SigfoxReceive(uint16_t timeOut_ms);
void infoSigfox();

#endif