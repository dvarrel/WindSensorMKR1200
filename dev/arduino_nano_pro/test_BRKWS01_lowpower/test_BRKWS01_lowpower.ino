#include <Arduino.h>
#include <LowPower.h>
#include "def.h"

SoftwareSerial Sigfox(rxPin, txPin); // RX, TX
uint8_t clock_divisor = 1;

void setup()
{
  fastClock();
  pinMode(pinWakeUp, OUTPUT);
  digitalWrite(pinWakeUp, HIGH);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  pinMode(pinGirb0, INPUT);
  pinMode(pinGirb1, INPUT);
  pinMode(pinGirb2, INPUT);
  pinMode(pinGirAlim, OUTPUT);
  digitalWrite(pinGirAlim, LOW);

  Serial.begin(115200);
  Sigfox.begin(9600); //module Sigfox 9600bauds
  delay_ms(1000);
  infoSigfox();
  delay_ms(1000);
  analogReference(INTERNAL);
  //first few readings from analogRead() may not be accurate
  for (uint8_t i=0;i<10;i++){
    analogRead(pinBat);
    delay_ms(1);
  }

  // start Counter T1
  TCCR1A = 0;
  TCCR1B =  bit (CS12) | bit (CS11) ;  // external clock source
  TCNT1=0;  

}

void loop() 
{
  slowClock();
  LowPower.idle(SLEEP_1S, ADC_OFF, TIMER2_OFF, TIMER1_ON, TIMER0_OFF, SPI_OFF, USART0_OFF, TWI_OFF);

  // normal operation 
  fastClock();
  digitalWrite(LED, 1);
  //COUNTER T1
  uint16_t c = ReadTCNT1();
  Serial.print("counter:"); Serial.println(c);
  
  //GIROUETTE
  digitalWrite(pinGirAlim, HIGH);
  delay_ms(1);
  uint8_t gray = digitalRead(pinGirb0)+digitalRead(pinGirb1)*2+digitalRead(pinGirb2)*4;
  digitalWrite(pinGirAlim, LOW);
  // code gray : Nord=110 NE=100 E=101 SE=001 S=000 SO=010 O=011 NO=111
  const uint8_t bin_val[8] = {4,3,5,6,1,2,0,7};
  Serial.print("girouette:");Serial.println(bin_val[gray] * 45);
  delay(500);

  //ANALOG A2 BATTERY
  uint16_t sum = 0;
  delay_ms(10);
  for (uint8_t i=0;i<N;i++){
    uint16_t value = analogRead(pinBat);
    sum = sum + value;
    //Serial.print(value);Serial.print(",");
    delay_ms(1);
  }  
  
  float Navg = sum / (float)N;
  //Serial.print(" N=");Serial.print(Navg);
  float Um= Navg*Uref/1024;
  float U = Um*Vdiv;
  Serial.print("VBAT:");Serial.print(U,3);Serial.println(" volts");
  delay_ms(10);
 
  digitalWrite(LED, 0);
}