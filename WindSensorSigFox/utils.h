#ifndef UTILS_H_
#define UTILS_H_
#include "defs.h"
#include "station.h"

extern Station station;

void setClock(uint8_t divisor);
#define fastClock() setClock(CPU_FULL)
#define slowClock() setClock(CPU_DIVISOR)
void delay_ms(uint32_t ms);
void SigfoxSendMessage(uint8_t len);
uint16_t ReadCounter(); 
void RAZcounter();

#ifdef ARDUINO_SAMD_MKRFox1200
extern volatile uint16_t count;
extern volatile uint16_t contactBounceTime;  // Timer to avoid contact bounce in interrupt routine
void isr_rotation();
void reboot();
#include <SigFox.h>

#else
#include <SoftwareSerial.h>
extern SoftwareSerial SerialSigfox;

void SigfoxSend(const char *ATcode);
char* SigfoxReceive(uint16_t timeOut_ms=200);
void SigfoxInfos();
void SigfoxWakeUp();
void SigfoxDeepSleep();
bool SigfoxConfigModule();

#include <avr/wdt.h>
void power_reduction_pins();
void power_reduction_peripherals();
#ifdef AVR_ATmega328PB
  #define PRR0 _SFR_MEM8(0x64)
  enum reg_prr0_t{
  //PRADC, PRUSART0, PRSPI0, PRTIM1, PRUSART1, PRTIM0, PRTIM2, PRTWI0
  PRSPI0=2, PRUSART1=4, PRTWI0=7
  };
  #define PRR1 _SFR_MEM8(0x65)
  enum reg_prr1_t{
  PRTIM3, PRSPI1=2, PRTIM4, PRPTC, PRTWI1
  };    
#endif
#endif

#endif
