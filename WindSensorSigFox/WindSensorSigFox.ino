/*
 NB: When flashing the MKR, it may be in slow CPU mode 
 which causes a non-detection of the USB COM port. 
 In order to wake it up, you need to tap twice on the "RST" button
 and the board will be redetected by your PC.

https://github.com/arduino-libraries/SigFox/issues/16
LED OFF during transmission : version >=1.0.5 avec Sigfox.debug(true);
Arduino SAMD boards version >= 1.8.13
 */
#include "utils.h"
#include "defs.h"
#include "station.h"

static uint32_t tickms_3s, timer;
static uint16_t nb_sigfox_day = 0;
Station station;
#ifdef ARDUINO_SAMD_MKRFox1200
uint16_t clock_divisor = CPU_DIVISOR;
volatile uint16_t count;
volatile uint16_t contactBounceTime;  // Timer to avoid contact bounce in interrupt routine
#else
#include <LowPower.h>
uint16_t clock_divisor = (uint16_t)pow(2,CPU_DIVISOR);
SoftwareSerial SerialSigfox(pinRx,pinTx);
#endif

void setup() {
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN,HIGH);
  pinMode(pinGirAlim,OUTPUT);
  digitalWrite(pinGirAlim,LOW);
  pinMode(pinAnemo, INPUT);
  #if BALISE_FFVL_AIRMES
  pinMode(pinGirb0, INPUT);
  pinMode(pinGirb1, INPUT);
  pinMode(pinGirb2, INPUT);
  #endif
  
  #ifdef ARDUINO_SAMD_MKRFox1200
  analogReadResolution(adcResolutionBits);
  attachInterrupt(digitalPinToInterrupt(pinAnemo), isr_rotation, RISING);
  #else
    #if !LSM100A || PCBVERSION > 0
      pinMode(pinWakeUp, OUTPUT);
      digitalWrite(pinWakeUp, HIGH);
    #endif
  power_reduction_pins();
  #endif
  analogReference(AnalogREF);

  slowClock();
  #if DEBUG || CALIBRATION
    if (clock_divisor > 1)
      #ifdef ARDUINO_SAMD_MKRFox1200
      Serial.begin((9600*clock_divisor));//9600*48=460800
      #else
      Serial.begin((57600*clock_divisor));//57600*16=921600
      #endif
    else
      Serial.begin(115200);
    for(uint8_t i=0; i<90; i++){
      if (Serial) break;
      delay_ms(10);
    } 
    Serial.println(F("Setup..."));
#endif
  delay_ms(100);
  station.init(DEBUG || CALIBRATION);
  delay_ms(100);
#ifdef ARDUINO_SAMD_MKRFox1200
  if (!SigFox.begin()) {
    #if DEBUG || CALIBRATION
      slowClock();
      Serial.println(F("Something wrong with SigFox module, rebooting..."));
      Serial.flush();
    #endif
    reboot();
  }
  #if DEBUG || CALIBRATION
    Serial.println(F("SigFox FW version ") + SigFox.SigVersion());
    Serial.println(F("ID:") + SigFox.ID());
    Serial.println(F("PAC:") + SigFox.PAC());
  #endif
#else
  SerialSigfox.begin(9600); //module Sigfox 9600bauds
  #if DEBUG || CALIBRATION
  SigfoxInfos();
  #endif
#endif
  SigfoxSendMessage(12);
  delay_ms(10);
#ifdef ARDUINO_SAMD_MKRFox1200
  count = 0;
#else
  TCCR1A = 0; // start Counter T1
  TCCR1B =  bit (CS12) | bit (CS11) ;  // external clock source
  TCNT1=0; 
#endif
#if CALIBRATION
  calibration();
#endif
  digitalWrite(LED_BUILTIN,LOW);
  timer = millis();
}

void loop() {
/*
Arduino pro mini : deepsleep with pulse anemo counter TIMER1 
millis() is stopped by TIMER0_OFF
*/
#if DEEPSLEEP && !ARDUINO_SAMD_MKRFox1200
  setClock(CLOCK_DIVISOR_256); 
  power_reduction_peripherals();
  LowPower.idle(SLEEP_1S, ADC_OFF, TIMER2_OFF, TIMER1_ON, TIMER0_OFF, SPI_OFF, USART0_OFF, TWI_OFF);
  power_reduction_peripherals();
  LowPower.idle(SLEEP_2S, ADC_OFF, TIMER2_OFF, TIMER1_ON, TIMER0_OFF, SPI_OFF, USART0_OFF, TWI_OFF);
  power_reduction_peripherals();
  fastClock();
  tickms_3s = MS_3S;
#else
  delay_ms(1);
  tickms_3s = (millis()-timer) * clock_divisor;
#endif
  if ( tickms_3s >= MS_3S ) {
    uint16_t c = ReadCounter();
    timer = millis();
#if DEBUG
    Serial.println(station.add_measure(c, tickms_3s));
    Serial.flush();
#else
    station.add_measure(c, tickms_3s);
#endif
  }

  if (station.get_nbmes() >= NB_5MIN && !station.is_first_compute()){ 
#if DEBUG
    Serial.println(station.compute_measures());
    char s[32];
    snprintf(s,sizeof(s),"nb_day=%u/%u", nb_sigfox_day,NB_SIGFOX_DAY);
    Serial.println(s);
    Serial.flush();
#else
    station.compute_measures(); //compute min, max, avg and encode all
#endif
    if ( ++nb_sigfox_day >= NB_SIGFOX_DAY){ // one message per day with extra infos
      nb_sigfox_day = 0;
      SigfoxSendMessage(12);
    }
    else SigfoxSendMessage(8);
    RAZcounter();
  }
  else if (station.get_nbmes() >= NB_5MIN && station.is_first_compute()) {
#if DEBUG
    Serial.println(station.compute_measures());
    Serial.flush();
#else
    station.compute_measures();//compute vmoy and gmoy and encode all
#endif
  }

}

#if !BALISE_FFVL_AIRMES
void calibration(){
  char s[128];
  static uint8_t k = 0;
  const char Cardinal[][nbPos]={"N","NE","E","SE","S","SO","O","NO"};
  Serial.println(F("mode calibration..."));
  Serial.println(F("placer la girouette dans la direction indiquée puis taper sur Entrée "));
  Serial.println(F("une fois toutes les directions saisies, vérifier l'affichage"));
  Serial.println(F("puis copier les valeurs{} dans le fichier def.h vers la ligne 18"));
  Serial.print(F("début dans:"));
  
  for ( uint8_t cpt=20; cpt>0; cpt--){
    if (Serial.available()) break;
    Serial.print(cpt);Serial.print(" ");
    delay_ms(1000);
  };
  Serial.println();

  while (true) {
    uint16_t sample = station.girouette_sample();

    if (Serial.available()) {
      station.n_gir[k] = sample;
      if (++k == nbPos) k = 0;
      while (Serial.available()) Serial.read();    
    }

    station.GirGap_init();

    uint16_t g_deg = station.girouette(sample);
    if (station.calibration_index==8){
      station.calibration_index = 0;
    }
    snprintf(s,sizeof(s),"k=%u CAN=%u Gap=%u angle=%u° direction=%s valeurs={",k,sample,station.GirGap,g_deg,Cardinal[station.calibration_index]);
    Serial.print(s);
    for(uint8_t i=0;i<nbPos;i++){
        Serial.print(station.n_gir[i]);
        if (i!=nbPos-1) Serial.print(", ");
    }
    snprintf(s,sizeof(s),"} taper Entrée pour la position %s ",Cardinal[k]);
    Serial.print(s);
    
    delay_ms(500);
    digitalWrite(LED_BUILTIN,HIGH);
    delay_ms(10);
    digitalWrite(LED_BUILTIN,LOW);

    uint32_t dT = (millis() - timer) * clock_divisor;
    if ( dT >= MS_3S )
    {
      uint16_t c = ReadCounter();
      timer = millis();
      float v_kmh = station.anemometre(c, dT);
      char vstr[6];;
      dtostrf(v_kmh,sizeof(vstr)-1, 1, vstr);
      trimChar(vstr);
      snprintf(s,sizeof(s)," v=%s km/h counter=%u",vstr, c);
      Serial.println(s);
      RAZcounter();
    }
    else Serial.println();
  }
}
#endif


