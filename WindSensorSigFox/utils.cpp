#include "Arduino.h"
#include "HardwareSerial.h"
#include "utils.h"
#include "defs.h"

void delay_ms(uint32_t ms){
  if(clock_divisor>1){
    if (ms > clock_divisor) {
      delayMicroseconds((uint32_t)(ms * 1000 / clock_divisor));
    }
    else{
      delay((uint32_t)(ms / clock_divisor));
    }
  }else{
    delay(ms);
  }
}

uint16_t ReadCounter()
{
  noInterrupts();
#ifdef ARDUINO_SAMD_MKRFox1200
  uint16_t c = count;
#else
  uint16_t c = TCNT1; //16bits counter on pin 5
  TCNT1 = 0;
#endif
  interrupts();
  return c;
}

void RAZcounter(){
  noInterrupts();
#ifdef ARDUINO_SAMD_MKRFox1200
  count = 0 ;
#else
  TCNT1 = 0;
#endif
  interrupts();
}

#ifndef ARDUINO_SAMD_MKRFox1200

void power_reduction_pins(){
  for (uint8_t i=0; i<sizeof(pins_unused); i++){
  pinMode(pins_unused[i],INPUT_PULLUP);
  }
}

void power_reduction_peripherals(){
  #ifdef AVR_ATmega328PB
  PRR0 = ( 1<<PRSPI0 | 1<<PRUSART1 | 1<<PRTIM2 );
  PRR1 = ( 1<<PRTIM3 | 1<<PRSPI1 | 1<<PRTIM4 | 1<<PRPTC | 1<<PRTWI1);
  #else
  PRR = (1<<PRTIM2 | 1<<PRSPI);
  #endif
}

void SigfoxSend(const char *ATcode){
  SerialSigfox.print(ATcode);
  SerialSigfox.print(F("\r"));
  SerialSigfox.flush();
}

char* SigfoxReceive(uint16_t timeOut_ms){
  static char str[256];
  memset(str, 0, sizeof(str));
  uint32_t end = millis() + (timeOut_ms / clock_divisor);
  uint8_t i = 0;
  while (millis() < end){
    while (SerialSigfox.available() && i<(sizeof(str)-1)){
      char c = SerialSigfox.read();
      if (c != 0x0A && c != 0x0D) { //0x0A Line feed | 0x0D Carriage return
        str[i] = c;
        i++;
      }
    }
  }
  if(i >= 2){
    if(str[i-2]=='O' && str[i-1]=='K'){
      str[i-2]=' ';
      str[i-1]='O';
      str[i]='K';
      i++;  
  }
  }
  str[i]='\0';
  return str;
}

void SigfoxWakeUp(){
  #if LSM100A
  //SigfoxSend("AT");
  #if DEBUG
  //Serial.print(F("AT="));Serial.println(SigfoxReceive());
  #endif
  #else
  digitalWrite(pinWakeUp, 0);
  delay_ms(100);
  digitalWrite(pinWakeUp, 1);
  #endif
}

void SigfoxDeepSleep(){
 #if LSM100A
  #else
  SigfoxSend("AT$P=2");
  #endif
#if DEBUG || CALIBRATION
  Serial.println(F("Module SigFox DeepSleep"));//deep sleep
  Serial.flush();
#endif
}

void SigfoxInfos(){
  #if LSM100A
  #define n 7
  const char* command[n] = {"AT", "AT+BAT=?", "AT+MODE=?", "AT+VER=?", "AT$ID", "AT$PAC", "ATS302=?"};
  const char* name[n] = {"test com", "module_voltage", "mode Sigfox(0) Lora(1)", "fw version", "ID", "PAC", "output power"};
  #else
  #define n 7
  const char* command[n] = {"AT", "AT$I=0", "AT$I=10", "AT$I=11", "ATS302?", "AT$T?" ,"AT$V?"};
  const char* name[n] = {"test com", "name", "ID", "PAC", "output Power", "module_temp","module_voltage"};
  #endif  
  
  Serial.println(F("SigfoxInfos"));
  SigfoxWakeUp();
  delay_ms(1000);
  char s[128];
  for (uint8_t i = 0; i < n; i++) {
    strcpy (s,command[i]);
    SigfoxSend(s);
    Serial.print(name[i]);
    Serial.print(F(" "));
    Serial.println(SigfoxReceive());
  }
  Serial.flush();
  SigfoxDeepSleep();
}

bool SigfoxConfigModule(){
  #ifdef LSM100A
  SigfoxSend("AT+MODE=?");
  uint16_t mode;
  sscanf(SigfoxReceive(), "%u", &mode);
  if (mode != 0){
    SigfoxSend("AT+MODE=0"); // 0:Sigfox 1:Lora
    //module reset on changing mode
    delay_ms(1);
    while (SerialSigfox.available()){
      SerialSigfox.read();
      delay_ms(1);
    }
    SigfoxSend("AT+MODE=?");
    sscanf(SigfoxReceive(), "%u", &mode);
    SigfoxSend("AT+VL=0"); // low Verbose Level,
    SigfoxReceive();
  }
  return mode == 0;
  #else
  SigfoxSend("ATS302?");Serial.print(F("Output Power:"));//Get Power Setting (7-15) default 14
  uint16_t power_dBm;
  sscanf(SigfoxReceive(), "%u", &power_dBm);
  Serial.println(power_dBm);
  if (power_dBm == 15)
    return true;
  else{
    Serial.println(F("Set Max Power"));
    SigfoxSend("ATS302=15"); //max power 14dBm
    Serial.println(SigfoxReceive());
    SigfoxSend("AT$WR"); //Save Config to flash
    Serial.println(SigfoxReceive());
  }
  #endif
}

#endif

void setClock(uint8_t divisor){
#ifdef ARDUINO_SAMD_MKRFox1200
  clock_divisor = divisor;
  if (CPU_DIVISOR != 1){
    GCLK->GENDIV.reg = GCLK_GENDIV_DIV(divisor) |         // Divide the 48MHz clock source by divisor 48: 48MHz/48=1MHz
                     GCLK_GENDIV_ID(0);            // Select Generic Clock (GCLK) 0
    while (GCLK->STATUS.bit.SYNCBUSY);               // Wait for synchronization      
  }
#else
  if ((clock_divisor==1 && CPU_DIVISOR!=0) || (clock_divisor>1 && CPU_DIVISOR==0)){
    noInterrupts();
    CLKPR = 0x80; // enable clock prescale change
    CLKPR = divisor;    // no prescale
    interrupts();
    clock_divisor = pow(2,divisor);
  }
#endif
}

void SigfoxSendMessage(uint8_t len) {
  fastClock();
  delay(10);
#if DEBUG || CALIBRATION
  Serial.println(F("Start sending SigFox frame..."));
#endif
#ifdef ARDUINO_SAMD_MKRFox1200
  SigFox.begin();
  delay(100);
  SigFox.debug(true);
  station.set_extra_infos();
  // Clears all pending interrupts
  SigFox.status();
  delay(50);
  delay(1);
  #if !CALIBRATION && !RADIO_OFF
  SigFox.beginPacket();
  SigFox.write((uint8_t*)&station.SigfoxWindMsg, len);
  station.SigfoxWindMsg.lastMessageStatus = SigFox.endPacket();
  #endif
  SigFox.end();
#else
  station.set_extra_infos();
  char s[32];
  snprintf(s,sizeof(s),"AT$SF=%s",station.get_sigfox_msg(len));
  SigfoxWakeUp();
  #if !CALIBRATION && !RADIO_OFF
   SigfoxSend(s);
   char *ret = SigfoxReceive(10000);
   #if DEBUG
     Serial.println(ret);
   #endif
   bool r = !(strcmp("OK",ret) == 0);
   station.SigfoxWindMsg.lastMessageStatus = r;
  #endif
  SigfoxDeepSleep();
#endif

#if DEBUG || CALIBRATION
  Serial.println(F("End sending SigFox frame..."));
  Serial.println(station.get_extra_infos());Serial.flush();
  Serial.println(station.get_sigfox_msg(len));Serial.flush();
#endif
  slowClock();
}

#ifdef ARDUINO_SAMD_MKRFox1200
void isr_rotation ()   {
  //noInterrupts();
  if ((micros() - contactBounceTime) > (BOUNCE_TIME/clock_divisor) ) {  // debounce the switch contact.
    count++;
    contactBounceTime = micros();
  }
  //interrupts();
}
#endif

void reboot() {
  #ifdef ARDUINO_SAMD_MKRFox1200
  NVIC_SystemReset();
  #else
  wdt_enable(WDTO_15MS);
  //asm volatile ("jmp 0x7800");
  #endif
  while (1) ;
}




