#ifndef __BMX280_HPP
#define __BMX280_HPP

#include <Arduino.h>
#include <Wire.h>

#define CHIP_ID_BMP280 (0x58)       // chip ID of BMP280
#define CHIP_ID_BME280 (0x60)       // chip ID of BME280
#define ERROR_OK (0x00)             // everything is fine
#define ERRORWire (0x01)            // some error with the two-wire bus
#define ERROR_SENSOR_TYPE (0x02)    // chip-ID doesn't match our expectations
#define RESET_KEY (0xB6)            // Reset value for reset register
#define STATUS_IM_UPDATE (0)        // im_update bit in status register
#define NB_ADDR 2

class BMX280 {
private:
  uint8_t _address;
  uint8_t _chipID;

  // calibration data
  int16_t _temperature[4];
  int16_t _pressure[10];
  int16_t _humidity[7];

  // fine temperature as global variable
  int32_t _BMX280t_fine;

  int16_t readTwoRegisters();
  int32_t readFourRegisters();
  uint8_t read8(uint8_t reg);
  uint8_t write8(uint8_t reg, uint8_t value);
  uint8_t setReg(uint8_t reg);

  virtual uint8_t applyOversamplingControls();
  virtual uint8_t readCalibrationData();

  enum class registers {
    CTRL_HUM = 0xF2,
    CTRL_MEAS = 0xF4,
    TEMP_CALIB = 0x88,       // temperature and pressure calibration data
    FIRST_HUM_CALIB = 0xA1,  // first byte of humidity calibration data
    SCND_HUM_CALIB = 0xE1,   // second part of humidity calibration data
    TEMP_MSB = 0xFA,
    HUM_MSB = 0xFD,
    PRESS_MSB = 0xF7,
    STATUS = 0xF3,
    CHIPID = 0xD0,  // Chip ID register
    RESET = 0xE0    // Reset register
  };

public:
  BMX280();
  uint8_t begin();
  uint8_t takeMeasurement();
  uint8_t getChipID();
  int32_t getTemperatureCelsius(const bool performMeasurement = false);
  uint32_t getPressure(const bool performMeasurement = false);
  uint32_t getRelativeHumidity(const bool performMeasurement = false);
};

#endif  //___BMX280_HPP