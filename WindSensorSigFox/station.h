#ifndef STATION_H_
#define STATION_H_

#include "Arduino.h"
#include "defs.h"
#include "BMX280.h"
#include "AHT20.h"

void trimChar(char * s);
#ifdef ARDUINO_SAMD_MKRFox1200 
char *dtostrf (double val, signed char width, unsigned char prec, char *sout);
#endif
// pour transmettre 2 périodes / message :
// message de 12 bytes
typedef struct __attribute__ ((packed)) sigfox_wind_message {
        uint8_t speedMin[2];
        uint8_t speedAvg[2];
        uint8_t speedMax[2];
        uint8_t directionAvg[2];
        uint8_t batteryVoltage;
        uint8_t pressure;
        int8_t temperature;
        uint8_t humidity;
        uint8_t lastMessageStatus;
} SigfoxWindMessage_t;
// /!\ avec ce format, il faut transmettre
// impérativement deux périodes de 5 minutes
// xxx[0] -> de T-10 à T-5 minutes
// xxx[1] -> de T-5 minutes à T
// T étant l'heure de transmission

class Station {
  private:
    uint16_t nb_mes = 0;
    bool first_compute = true;
    float sum_v;
    double sum_sin_g,sum_cos_g;
    float v_kmh_min[2] = {999,999};        
    float v_kmh_avg[2];        
    float v_kmh_max[2];        
    uint16_t g_deg_avg[2];
    float batteryVoltage;
    float pressure=1017;
    float temperature,humidity;
    uint8_t encodeWindSpeed (float speedKmh);
    uint8_t encodeWindDirection (uint16_t g_deg);
    uint8_t encodeBatteryVoltage (float v);
    int8_t encodeTemperature(float t);
    uint8_t encodePressure(float p);
    uint8_t encodehumidity(float h);
    bool _debug;
    bool bmx280_present = false;
    BMX280 bmx280; //I2C
    bool aht20_present = false;
    AHT20 aht20;; //I2C
    void readBatteryVoltage();
    void readExtraSensors();

  public:
    SigfoxWindMessage_t  SigfoxWindMsg;
    void init(bool _debug=false);
    uint16_t get_nbmes();
    bool is_first_compute();
    void set_extra_infos();
    char* get_extra_infos();
    char* get_sigfox_msg(uint8_t len);
    char* compute_measures();
    char* add_measure(uint16_t count, uint32_t deltaT);
    float anemometre(uint16_t count, uint32_t deltaT);
    uint16_t girouette_sample();
    uint16_t girouette(uint16_t sample);
    uint16_t GirGap = 0xFFFF;
    void GirGap_init();
    uint16_t n_gir[nbPos]{0, 0, 0, 0, 0, 0, 0, 0};
    uint8_t calibration_index;
};

#endif
