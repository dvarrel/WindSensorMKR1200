#ifndef DEFS_H_
#define DEFS_H_
#include "Arduino.h"

#define CALIBRATION false // mettre à true pour la calibration : suivre les instructions dans le terminal, une fois le programme téléversé
#define DEBUG false // affichage dans la console + réduction des périodes
#define DEEPSLEEP true // reduction conso arduino pro mini
#define RADIO_OFF false // no transmission,for debub only

//hardware
#define BALISE_FFVL_AIRMES false //mettre à true si balise ffvl AIRMES
#define LSM100A true // false pour module BRKWS01
#define AVR_ATmega328PB

#define SOFTVERSION 3
#define PCBVERSION 0
#define VOC_SOLAR 63
#define VCC_ALIM 18

#define nbPos 8      // 8 capteurs = 8 positions N,NE,E,SE,S,SO,O,NO
#if !BALISE_FFVL_AIRMES && !CALIBRATION
// saisir les 8 valeurs issues de la calibration dans NGIR
const uint16_t NGIR[nbPos] = {785, 459, 91, 183, 286, 629, 943, 885};//ID9 LSM100A
//const uint16_t NGIR[nbPos] = {3125, 1931, 682, 986, 1330, 2546, 3746, 3518}; //ID1 928
//const uint16_t NGIR[nbPos] = {3145, 1962, 690, 994, 1344, 2559, 3766, 3538}; //ID2 931
//const uint16_t NGIR[nbPos] = {3103, 1890, 658, 951, 1289, 2504, 3750, 3507}; //ID3 1207 FFVL 
//const uint16_t NGIR[nbPos] = {3103, 1890, 658, 951, 1289, 2504, 3750, 3507}; //ID6 925 villes 
//const uint16_t NGIR[nbPos] = {3156, 1952, 686, 991, 1342, 2566, 3781, 3549}; //ID7 1216 apremont  
//const uint16_t NGIR[nbPos] = {784, 460, 92, 184, 286, 629, 944, 886};//ID8 1215 adison
#endif

#ifdef ARDUINO_SAMD_MKRFox1200  
  #define pinGirAlim 5 // alimentation du réseau de capteurs
  #define pinAnemo  4  //entrée interruption
  #define pinSolarAdc   A1  
  #define pinBatAdc     A3  
  #if BALISE_FFVL_AIRMES
    #define pinGirb0 A1 //bit 0 codeur absolu fil jaune
    #define pinGirb1 A2 //bit 1 codeur absolu fil vert
    #define pinGirb2 A6 //bit 2 codeur absolu fil bleu 
  #else
    #define pinGirAdc A2 // entrée analogique des 8 capteurs reed
  #endif
#else
  #define pinSolarAdc   A1
  #define pinBatAdc     A2  
  #define pinGirAlim A3          // alimentation du réseau de capteurs
  #define pinTx 4 //Rx_Sigfox
  #define pinAnemo  5     // counter T1 
  #define pinRx 6 //Tx_Sigfox module BRKWS01 LSM100A
  #if LSM100A
    #if PCBVERSION > 0
      #define pinWakeUp 9  //LSM100A 15
    #endif
    #define pinNRST 7  //LSM100A 30
  #else
    #define pinWakeUp 7  //BRKWS01 GPIO9
  #endif
  #if BALISE_FFVL_AIRMES
    #define pinGirb0 3 //bit 0 codeur absolu fil jaune
    #define pinGirb1 8 //bit 1 codeur absolu fil vert
    #define pinGirb2 A0 //bit 2 codeur absolu fil bleu
    const uint8_t pins_unused[] = {2, 9, 10, 11, 12};
  #else
    #define pinGirAdc A0
    #if PCBVERSION == 0
    const uint8_t pins_unused[] = {2, 3, 8, 9, 10, 11, 12 };
    #else
    const uint8_t pins_unused[] = {2, 3, 8, 10, 11, 12 };
    #endif
  #endif
#endif


#ifdef ARDUINO_SAMD_MKRFox1200  
  #define AnalogREF AR_DEFAULT  //ref voltage 3.3V
  #define adcResolutionBits 12  
#else
  #define AnalogREF DEFAULT  //ref voltage 3.3V 
  #define adcResolutionBits 10 
#endif
const float Vref = VCC_ALIM/10.;
#if VCC_ALIM > 30
#define R5 1000
#define R6 1000
//voltage divisor R6=R5 Max Battery or Solar Voltage = 6.6V
#else
#define R5 2700
#define R6 1000
//voltage divisor Max Battery or Solar Voltage = 2*3=6V
#endif
#if (R6+R5)*VCC_ALIM < VOC_SOLAR*R6
 #error message "resistors do not match Vmax Voltage"
#endif
const float Vdiv = ((float)R6+(float)R5)/(float)R6;
const uint16_t Nmax = (uint16_t)pow(2, adcResolutionBits) - 1u;
#define encodedDeltaVoltage -2

// échantillonnage 3sec d'après WMO - delta T = 5 min
// en ms
#define MS_3S 3000u
#if DEBUG
  #define NB_5MIN 4u
  #define NB_SIGFOX_DAY 5u
#else
  #define NB_5MIN 100u // 100 mesures * 3s = 5min
  #define NB_SIGFOX_DAY 140u // abonnement Sigfox 140 octets/jour 6*24=144 dépassement!
#endif

//anemometre
// capteur reed(misol) ou photo(airmes fil blanc)
#if BALISE_FFVL_AIRMES
  #define TIPTOUR 8       // 8 réflecteurs
#else
  #define TIPTOUR 2     // 2 aimants par tour
#endif
#define R         0.07  //rayon coupelles m
#define ANEMO_COEF 3.  //correcteur
#define BOUNCE_TIME 500 // µs https://www.reed-sensor.com/reed-switches/

const uint16_t angleSlice = 360/nbPos;
#define DirectionGap 0 // calibrage girouette

//temperature, humidity, pressure
#define encodedGapPressure -850

extern uint16_t clock_divisor;
#ifdef ARDUINO_SAMD_MKRFox1200
#define CPU_FULL 1
#define CPU_DIVISOR 48 // cpu clock to reduce power 2mA@1Mhz / 14mA@48Mhz
#define Serial Serial1
#else
enum clock_prescaler_t
{
	CLOCK_DIVISOR_1,CLOCK_DIVISOR_2,CLOCK_DIVISOR_4,CLOCK_DIVISOR_8,CLOCK_DIVISOR_16,
  CLOCK_DIVISOR_32,CLOCK_DIVISOR_64,CLOCK_DIVISOR_128,CLOCK_DIVISOR_256
};
#define CPU_FULL CLOCK_DIVISOR_1
#define CPU_DIVISOR CLOCK_DIVISOR_1 
/* see datasheet : Active Supply Current vs. Frequency @3.6V : 3mA@8MHz 0.4mA@1MHz 0.1mA@0.25MHz
-> no Gain to reduce frequency */
#endif

#if DEBUG 
  #warning message "DEBUG MODE"
#endif
#if CALIBRATION
  #warning message "CALIBRATION MODE"
#endif

#endif
