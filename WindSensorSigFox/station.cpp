#include "HardwareSerial.h"
#include <Arduino.h>
#include <math.h>
#include "station.h"

void Station::init(bool _debug) {
  this->_debug = _debug;
#if !BALISE_FFVL_AIRMES  
  #if !CALIBRATION
  for (uint8_t i=0;i<nbPos;i++){
    n_gir[i] = NGIR[i];
  }
  GirGap_init();
  #endif
#endif
  Wire.begin();
  aht20_present = aht20.begin();
  bmx280_present = (bmx280.begin() == ERROR_OK);
  if (_debug) {
    if (!bmx280_present){
      Serial.println(F("Could not find BMx280 sensor..."));
    }
    else {
      Serial.print(F("Sensor found : "));
      if (bmx280.getChipID()==CHIP_ID_BME280) Serial.println(F("BME280"));
      else Serial.println(F("BMP280"));
    }
    if (!aht20_present){
      Serial.println(F("Could not find AHT20 sensor..."));
      delay(50);
    }
    else {
      Serial.println(F("Sensor found : AHT20"));
    }
  }
  if (bmx280_present || aht20_present){
    readExtraSensors();    
  }
  if (_debug){
    Serial.println(F("station init end"));
    Serial.flush();
  }
}

char* Station::add_measure(uint16_t count, uint32_t deltaT){
  nb_mes++;
  uint8_t i = !first_compute;
  uint16_t g_deg = girouette(girouette_sample());
  float v = anemometre(count,deltaT);
  static char str1[128];
  memset(str1, 0, sizeof(str1));
  if(_debug){
    char vstr[6];
    dtostrf(v,sizeof(vstr)-1, 1, vstr);
    trimChar(vstr);
    snprintf(str1,sizeof(str1),"%u_%u c=%u v=%s g=%u", i, nb_mes, count, vstr, g_deg);
  }
  if (v_kmh_min[i] > v) v_kmh_min[i] = v;
  if (v_kmh_max[i] < v) v_kmh_max[i] = v;
  sum_v += v;

  sum_sin_g += v * sin(g_deg*PI/180.);
  sum_cos_g += v * cos(g_deg*PI/180.);
  return str1;
}

char* Station::compute_measures(){
  static char str2[128];
  memset(str2, 0, sizeof(str2));
  uint8_t i = !first_compute;
  v_kmh_avg[i] = sum_v / nb_mes;
  double g = atan2(sum_sin_g , sum_cos_g)*180./PI;
  if (g<0) g += 360.;
  g_deg_avg[i] = (uint16_t)g;
  SigfoxWindMsg.speedMin[i] = encodeWindSpeed(v_kmh_min[i]);
  SigfoxWindMsg.speedAvg[i] = encodeWindSpeed(v_kmh_avg[i]);
  SigfoxWindMsg.speedMax[i] = encodeWindSpeed(v_kmh_max[i]);
  SigfoxWindMsg.directionAvg[i] = encodeWindDirection(g_deg_avg[i]);
  if (_debug) {
    char vsmin[6];char vsavg[6];char vsmax[6];
    dtostrf(v_kmh_min[i],sizeof(vsmin)-1, 1, vsmin);
    trimChar(vsmin);
    dtostrf(v_kmh_avg[i],sizeof(vsavg)-1, 1, vsavg);
    trimChar(vsavg);
    dtostrf((double)v_kmh_max[i],sizeof(vsmax)-1, 1, vsmax);
    trimChar(vsmax);
    char format[]="v_min=%s v_avg=%s v_max=%s g_avg=%u";
    snprintf(str2, sizeof(str2), format, vsmin,vsavg,vsmax,g_deg_avg[i]);
  }
  v_kmh_min[i]=999;
  v_kmh_max[i]=0;
  sum_v =0; sum_sin_g=0; sum_cos_g=0;
  nb_mes = 0;
  first_compute = !first_compute;
  return str2;
}

uint16_t Station::get_nbmes(){
  return this->nb_mes;
}

bool Station::is_first_compute(){
  return this->first_compute;
}

float Station::anemometre(uint16_t count, uint32_t deltaT){
    float freq = count / ( TIPTOUR * deltaT / 1000.) ;
    float v = 2. * PI * freq * R * 3.6 * ANEMO_COEF;
    return v;
}

void Station::GirGap_init(){
  GirGap = 0xFFFF;
  for(uint8_t i=0; i<nbPos; i++){
    for(uint8_t j=0; j<nbPos; j++){
      if (n_gir[i]!=0 && n_gir[j]!=0){
        uint16_t gap = abs(n_gir[i] - n_gir[j]);
        if ( GirGap > gap && gap > 0 ) GirGap = gap;
      }
    }
  }
}


uint16_t Station::girouette_sample(){
  uint16_t sample = 0;
  digitalWrite(pinGirAlim,HIGH);
  delayMicroseconds(10000/clock_divisor);
  #if BALISE_FFVL_AIRMES
    uint8_t gray = digitalRead(pinGirb0)+digitalRead(pinGirb1)*2+digitalRead(pinGirb2)*4;
    // code gray : Nord=110 NE=100 E=101 SE=001 S=000 SO=010 O=011 NO=111
    const uint8_t bin_val[8] = {4,3,5,6,1,2,0,7};
    sample = bin_val[gray] * angleSlice;
  #else
    const uint8_t n = 16;
    for(uint8_t i=0;i<n;i++){
      delayMicroseconds(5000/clock_divisor);
      sample += analogRead(pinGirAdc);
    }
    sample /= n;
  #endif
  digitalWrite(pinGirAlim,LOW);
  return sample;
}


uint16_t Station::girouette(uint16_t sample){
  #if BALISE_FFVL_AIRMES
  return sample;
  #else  
    uint16_t delta = GirGap / 3;
    for(uint8_t i=0;i<nbPos;i++){
      if ( sample < (n_gir[i]+delta) && sample > (n_gir[i]-delta)){
        calibration_index = i;
        return (i * angleSlice) + DirectionGap;
      }
    }
    return 0;
  #endif
}


char * Station::get_extra_infos(){
  static char str3[128];
  memset(str3, 0, sizeof(str3));
  char s_ubat[6];
  float ubat = (SigfoxWindMsg.batteryVoltage/100.)-encodedDeltaVoltage;
  dtostrf(ubat,sizeof(s_ubat)-1, 2, s_ubat);
  trimChar(s_ubat);
  const char* format ="Ubat=%sV T=%d°C P=%uhPa H=%u%% sigfox_last_error=%u";
  snprintf(str3, sizeof(str3), format, s_ubat,
  SigfoxWindMsg.temperature, (SigfoxWindMsg.pressure - encodedGapPressure),
  (SigfoxWindMsg.humidity>>1), SigfoxWindMsg.lastMessageStatus);
  return str3;
}

char* Station::get_sigfox_msg(uint8_t len){
  static char msg[32];
  memset(msg, 0, sizeof(msg));
  snprintf(msg, sizeof(msg), "%02X%02X%02X%02X%02X%02X%02X%02X",
    SigfoxWindMsg.speedMin[0],SigfoxWindMsg.speedMin[1],
    SigfoxWindMsg.speedAvg[0],SigfoxWindMsg.speedAvg[1],
    SigfoxWindMsg.speedMax[0],SigfoxWindMsg.speedMax[1],
    SigfoxWindMsg.directionAvg[0],SigfoxWindMsg.directionAvg[1]);
  if (len==12) {
    snprintf(msg+strlen(msg), sizeof(msg)-strlen(msg), "%02X%02X%02X%02X",
    SigfoxWindMsg.batteryVoltage,
    SigfoxWindMsg.pressure,
    SigfoxWindMsg.temperature,
    SigfoxWindMsg.humidity);
  }
  return msg;
}

void Station::readBatteryVoltage() {
  delayMicroseconds(10000/clock_divisor);
  uint16_t N = 0;
  const uint8_t n = 8;
  for (uint8_t i=0; i<n; i++){
      N += analogRead(pinBatAdc);
      delayMicroseconds(5000/clock_divisor);
  }
  N /= n;
  float Vadc = N * Vref / (Nmax);
  batteryVoltage =  Vdiv * Vadc;
  SigfoxWindMsg.batteryVoltage = encodeBatteryVoltage(batteryVoltage);
}

void Station::readExtraSensors(){
  if(aht20_present){
    humidity = aht20.getHumidity();
    temperature = aht20.getTemperature();
  }
  if(bmx280_present){
    bmx280.takeMeasurement();
    pressure = bmx280.getPressure() / 100.;
    if (!aht20_present){
      temperature = bmx280.getTemperatureCelsius() / 100.;
      humidity = bmx280.getRelativeHumidity() / 100.;
    }
  }
  if ((bmx280_present || aht20_present) && _debug){
    char s[32];
    snprintf(s,sizeof(s),"P=%uhPa T=%d°C H=%u%%",(uint16_t)pressure,(int8_t)temperature,(uint8_t)humidity);
    Serial.println(s);
  }
}

void Station::set_extra_infos(){
  readBatteryVoltage();
  readExtraSensors();
  SigfoxWindMsg.temperature = encodeTemperature(this->temperature);
  SigfoxWindMsg.pressure = encodePressure(this->pressure);
  SigfoxWindMsg.humidity = encodehumidity(this->humidity);
  // add last error to pressure byte -> error=0 pressure even else odd
  if (SigfoxWindMsg.lastMessageStatus==0)
    SigfoxWindMsg.pressure &= 0xFE;
  else
    SigfoxWindMsg.pressure |= 0x01;
  //add softversion to humidity
  SigfoxWindMsg.humidity = (SigfoxWindMsg.humidity<<1) + SOFTVERSION;
}

// encodage vent sur 1 octet (code original du Pioupiou)
uint8_t Station::encodeWindSpeed (float speedKmh) {
  uint8_t encodedSpeed;
  if (speedKmh < 10.) {
    // 0 to 9.75 kmh : 0.25 km/h resolution
    encodedSpeed = (uint8_t)(float)(speedKmh * 4. + 0.5);
  } else if (speedKmh < 80.) {
    // 10 to 79.5 kmh  : 0.5 km/h resolution
    encodedSpeed = (uint8_t)(float)(speedKmh * 2. + 0.5) + 20;
  } else if (speedKmh < 120.) {
    // 80 to 119 kmh  : 1 km/h resolution
    encodedSpeed = (uint8_t)(float)(speedKmh + 0.5) + 100;
  } else if (speedKmh < 190.) {
    // 120 to 188 kmh  : 2 km/h resolution
    encodedSpeed = (uint8_t)(float)(speedKmh / 2. + 0.5) + 160;
  } else {
    // 190 or + : out of range
    encodedSpeed = 0xFF;
  }
  return encodedSpeed;
}

// encodage direction sur 1 byte ( degres / 2 )
uint8_t Station::encodeWindDirection (uint16_t g_deg) { // degres
  return (uint8_t)(uint16_t)(g_deg / 2);
}

// encodage tension batterie sur 1 octet ( -2. * 100 )
uint8_t Station::encodeBatteryVoltage (float v) {
  return (uint8_t)(float)((v + encodedDeltaVoltage) * 100.);
}

// encodage temperature 1 octet signé (-128 + 127 )
int8_t Station::encodeTemperature(float t) {
  return (int8_t)(round(t));
}
// encodage pression sur 1 octet (hPa -850)
uint8_t Station::encodePressure(float p) {
  return (uint8_t)(uint16_t)(round(p) + encodedGapPressure);
}
// encodage humidité relative sur 1 octet (0-100 %)
uint8_t Station::encodehumidity(float h) {
  return (uint8_t)(round(h));
}


void trimChar(char * s){
  char ts[128];
  strcpy (ts,s);
  uint8_t i=0;
  while(ts[i]==' ')i++; 
  uint8_t j=0;
  while(ts[i]!='\0'){
    s[j]=ts[i];
    i++;
    j++;
  }
  s[j]='\0';
}

#if defined(ARDUINO_ARCH_SAMD)
char *dtostrf (double val, signed char width, unsigned char prec, char *sout)
{
  asm(".global _printf_float");
  char fmt[20];
  sprintf(fmt, "%%%d.%df", width, prec);
  sprintf(sout, fmt, val);
  return sout;
}
#endif /* defined(ARDUINO_ARCH_SAMD) */
