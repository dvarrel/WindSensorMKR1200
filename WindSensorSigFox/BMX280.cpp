#include "BMX280.h"

/// \brief
/// Constructor of BMX280
/// \details
/// This creates a BMX280 object
/// and the address of the chip to communicate with.
BMX280::BMX280() {
}

/// \brief
/// Read 16 bits
/// \details
/// This function reads 16 bits from the I2C bus.
int16_t BMX280::readTwoRegisters() {
  uint8_t lo = Wire.read();
  uint8_t hi = Wire.read();
  return hi << 8 | lo;
}

/// \brief
/// Read 32 bits
/// \details
/// This function reads 32 bits from the I2C bus.
int32_t BMX280::readFourRegisters() {
  uint8_t msb = Wire.read();
  uint8_t lsb = Wire.read();
  uint8_t xlsb = Wire.read();
  return (uint32_t)msb << 12 | (uint32_t)lsb << 4 | (xlsb >> 4 & 0x0F);
}

/// \brief
/// Read 8 bits
/// \details
/// This function reads 8 bits from the register (reg) via I2C bus.
uint8_t BMX280::read8(uint8_t reg) {
  Wire.beginTransmission(_address);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(_address, (byte)1);
  return Wire.read();
}

/// \brief
/// Write 8 bits
/// \details
/// This function writes 8 bits (value) to the device into to register (reg).
/// Returns an error code if there was one from the bus
uint8_t BMX280::write8(uint8_t reg, uint8_t value) {
  Wire.beginTransmission(_address);
  Wire.write(reg);
  Wire.write(value);
  if (Wire.endTransmission()) return ERRORWire;

  return ERROR_OK;
}

/// \brief
/// Set register
/// \details
/// This function selects a register (reg) of the sensor.
/// Returns an error code if there was one from the bus
uint8_t BMX280::setReg(uint8_t reg) {
  Wire.beginTransmission(_address);
  Wire.write(reg);
  if (Wire.endTransmission()) return ERRORWire;

  return ERROR_OK;
}


/// \brief
/// begin
/// \details
/// This applies the set oversampling controls and reads the calibration
/// data from the register. 
/// Returns an error code if there was one from the bus the chipID is not matching BME280 or BMP280
uint8_t BMX280::begin() {
  const uint8_t BMX280_I2C_ADDR[NB_ADDR]={0x76, 0x77};
  // Check that something is attached to the bus at the given address
  bool find = false;
  for (uint8_t i=0; i<NB_ADDR; i++){
    _address = BMX280_I2C_ADDR[i];
    Wire.beginTransmission(_address);
    if (Wire.endTransmission()==0){
      find = true;
      break;
    }
  }
  if (!find) return ERRORWire;

  

  // Read chip ID
  _chipID = read8((uint8_t)registers::CHIPID);

  // Check sensor ID BMP280 or BME280
  if ((_chipID != CHIP_ID_BMP280) && ((_chipID != CHIP_ID_BME280))) {
    // BMP280 / BME280 not found
    return ERROR_SENSOR_TYPE;
  }

  // Generate soft-reset
  if (write8((uint8_t)registers::RESET, (uint8_t)RESET_KEY)) return ERRORWire;

  // Wait for copy completion NVM data to image registers
  delay(10);
  while ((read8((uint8_t)registers::STATUS) & (bit(STATUS_IM_UPDATE)))) {
    delay(10);
  }

  // set mode of sensor
  if (applyOversamplingControls()) return ERRORWire;

  // read factory trimming parameters
  if (readCalibrationData()) return ERRORWire;

  return ERROR_OK;
}

/// \brief
/// getChipID
/// \details
/// Chip ID as read with begin()
uint8_t BMX280::getChipID() {
  // Return chip ID
  return _chipID;
}

/// \brief
/// Take  measurement
/// \details
/// This function takes a  measurement. That is, the BME280 is woken up to take
/// a measurement after which it goes back to sleep. During this sleep, it consumes
/// 0.25uA!
/// Returns an error code if there was one from the bus
uint8_t BMX280::takeMeasurement() {

  // ctrl_meas - see datasheet section 5.4.5
  //  mode: ctrl_meas[0..1] 0b01 (0b11 for normal and 0b00 for sleep mode)
  // pressure oversampling x 1: ctrl_meas[4..2] 0b001
  // temperature oversampling x 1: ctrl_meas[7..5] 0b001
  if (write8((uint8_t)registers::CTRL_MEAS, 0b00100101)) return ERRORWire;
  return ERROR_OK;
}

/// \brief
/// Apply oversampling controls
/// \details
/// This function sets the sampling controls to values that are
/// suitable for all kinds of applications.
/// Returns an error code if there was one from the bus
uint8_t BMX280::applyOversamplingControls() {
  // humidity oversampling - see datasheet section 5.4.3
  // oversampling x 1: 0x01
  // only to be set when a BME280 is used
  if (_chipID == CHIP_ID_BME280) {
    if (write8((uint8_t)registers::CTRL_HUM, 0b00000001)) return ERRORWire;
  }

  // ctrl_meas - see datasheet section 5.4.5
  //  mode: ctrl_meas[0..1] 0b01 (0b11 for normal and 0b00 for sleep mode)
  // pressure oversampling x 1: ctrl_meas[4..2] 0b001
  // temperature oversampling x 1: ctrl_meas[7..5] 0b001
  if (write8((uint8_t)registers::CTRL_MEAS, 0b00100101)) return ERRORWire;
  return ERROR_OK;
}

/// \brief
/// Read Calibrations
/// \details
/// This functions reads the calibration data after which it is
/// stored in the temperature, pressure and humidity arrays for
/// later use in compensation.
/// Returns an error code if there was one from the bus
uint8_t BMX280::readCalibrationData() {
  if (setReg((uint8_t)registers::TEMP_CALIB)) return ERRORWire;
  Wire.requestFrom(_address, (uint8_t)24);
  // read 24 bytes for temperature and pressure calibration data
  for (int i = 1; i <= 3; i++) _temperature[i] = readTwoRegisters();  // Temperature
  for (int i = 1; i <= 9; i++) _pressure[i] = readTwoRegisters();     // Pressure

  // read humidity calibration data in case its really a BME280
  if (_chipID == CHIP_ID_BME280) {
    // read 1. byte of humidity calibration data
    _humidity[1] = read8((uint8_t)registers::FIRST_HUM_CALIB);

    // read second part of humidity calibration data
    if (setReg((uint8_t)registers::SCND_HUM_CALIB)) return ERRORWire;
    Wire.requestFrom(_address, (uint8_t)7);
    _humidity[2] = readTwoRegisters();
    _humidity[3] = (uint8_t)Wire.read();
    uint8_t e4 = Wire.read();
    uint8_t e5 = Wire.read();
    _humidity[4] = ((int16_t)((e4 << 4) + (e5 & 0x0F)));
    _humidity[5] = ((int16_t)((Wire.read() << 4) + ((e5 >> 4) & 0x0F)));
    _humidity[6] = ((int8_t)Wire.read());
  }

  // get temperature reading to initialize BMX280t_fine
  if (takeMeasurement()) return ERRORWire;
  getTemperatureCelsius();

  // done
  return ERROR_OK;
}
/// \brief
/// Get Temperature Celsius
/// \details
/// This function retrieves the compensated temperature
int32_t BMX280::getTemperatureCelsius(const bool performMeasurement) {
  Wire.beginTransmission(_address);
  if (performMeasurement) {
    Wire.write((uint8_t)registers::CTRL_MEAS);
    Wire.write(0b00100101);
  }
  Wire.write((uint8_t)registers::TEMP_MSB);
  Wire.endTransmission();
  Wire.requestFrom(_address, (uint8_t)3);
  int32_t adc = readFourRegisters();
  int32_t var1 = ((((adc >> 3) - ((int32_t)((uint16_t)_temperature[1]) << 1))) * ((int32_t)_temperature[2])) >> 11;
  int32_t var2 = ((((adc >> 4) - ((int32_t)((uint16_t)_temperature[1]))) * ((adc >> 4) - ((int32_t)((uint16_t)_temperature[1])))) >> 12);
  var2 = (var2 * ((int32_t)_temperature[3])) >> 14;
  _BMX280t_fine = var1 + var2;
  int32_t temperature = (_BMX280t_fine * 5 + 128) >> 8;

  return temperature;
}

/// \brief
/// Get Pressure
/// \details
/// This function retrieves the compensated pressure
uint32_t BMX280::getPressure(const bool performMeasurement) {
  Wire.beginTransmission(_address);
  if (performMeasurement) {
    Wire.write((uint8_t)registers::CTRL_MEAS);
    Wire.write(0b00100101);
  }
  Wire.write((uint8_t)registers::PRESS_MSB);
  Wire.endTransmission();
  Wire.requestFrom(_address, (uint8_t)3);

  int32_t adc = readFourRegisters();
  int32_t var1 = (((int32_t)_BMX280t_fine) >> 1) - (int32_t)64000;
  int32_t var2 = (((var1 >> 2) * (var1 >> 2)) >> 11) * ((int32_t)_pressure[6]);
  var2 = var2 + ((var1 * ((int32_t)_pressure[5])) << 1);
  var2 = (var2 >> 2) + (((int32_t)_pressure[4]) << 16);
  var1 = (((_pressure[3] * (((var1 >> 2) * (var1 >> 2)) >> 13)) >> 3) + ((((int32_t)_pressure[2]) * var1) >> 1)) >> 18;
  var1 = ((((32768 + var1)) * ((int32_t)((uint16_t)_pressure[1]))) >> 15);

  if (var1 == 0) {
    return 0;
  }
  uint32_t p = (((uint32_t)(((int32_t)1048576) - adc) - (var2 >> 12))) * 3125;
  if (p < 0x80000000) {
    p = (p << 1) / ((uint32_t)var1);
  } else {
    p = (p / (uint32_t)var1) * 2;
  }

  var1 = (((int32_t)_pressure[9]) * ((int32_t)(((p >> 3) * (p >> 3)) >> 13))) >> 12;
  var2 = (((int32_t)(p >> 2)) * ((int32_t)_pressure[8])) >> 13;
  p = (uint32_t)((int32_t)p + ((var1 + var2 + _pressure[7]) >> 4));

  return p;
}

/// \brief
/// Get Humidity
/// \details
/// This function retrieves the compensated humidity 
uint32_t BMX280::getRelativeHumidity(const bool performMeasurement) {
  // silently bail out if it is the wrong type of sensor
  if (_chipID != CHIP_ID_BME280) return 0;

  Wire.beginTransmission(_address);
  if (performMeasurement) {
    Wire.write((uint8_t)registers::CTRL_MEAS);
    Wire.write(0b00100101);
  }
  Wire.write((uint8_t)registers::HUM_MSB);
  Wire.endTransmission();
  Wire.requestFrom(_address, (uint8_t)2);
  uint8_t hi = Wire.read();
  uint8_t lo = Wire.read();
  int32_t adc = (uint16_t)(hi << 8 | lo);
  int32_t var1;
  var1 = (_BMX280t_fine - ((int32_t)76800));
  var1 = (((((adc << 14) - (((int32_t)_humidity[4]) << 20) - (((int32_t)_humidity[5]) * var1)) + ((int32_t)16384)) >> 15) * (((((((var1 * ((int32_t)_humidity[6])) >> 10) * (((var1 * ((int32_t)_humidity[3])) >> 11) + ((int32_t)32768))) >> 10) + ((int32_t)2097152)) * ((int32_t)_humidity[2]) + 8192) >> 14));
  var1 = (var1 - (((((var1 >> 15) * (var1 >> 15)) >> 7) * ((int32_t)_humidity[1])) >> 4));
  var1 = (var1 < 0 ? 0 : var1);
  var1 = (var1 > 419430400 ? 419430400 : var1);
  uint32_t humidity = (uint32_t)((var1 >> 12) * 25) >> 8;
  return humidity;
}

