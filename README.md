## Balise météo open-source pour <a href="https://openwindmap.org/">https://openwindmap.org/</a>

<img src="img/new_pcb.webp" height="250px"/>

## Sommaire
- [Vue 3D et plans](#3d-et-plans)
- [Caractéristiques](#caractéristiques-)
- [Composants](#composants-liste-dachat-)
- [Assemblage carte électronique](#assemblage-carte-électronique)
- [Recommendations](#recommendations)
- [Programmation](#programmation)

## 3D et plans
- <a href="https://cad.onshape.com/documents/6b13ef821e263382372072eb/w/90bcda648e57637a9ae0e956/e/43a202e3448bb59f5c78bd82">modèle 3D libre onshape</a>
<img src="img/ensemble.png"/> 

- carte électronique montée
<img src="img/carte.webp" height="250px"/>

- <a href="https://gitlab.com/dvarrel/WindSensorMKR1200/blob/main/img/support.pdf" target="_blank">support</a>
<img src="img/support.png" height="250px"/>

## Caractéristiques :
- low power @1Mhz : 2mA (voir le doc [notice_calculs.ods](dev/notice_calculs.ods))
- librairie arduino sigfox modifiée pour empêcher clignotement LED lors de l'envoi
- avec une pile 18650 (2000mAh), 36j d'autonomie sans soleil
- panneau solaire 5W -> recharge pile sous 4h de soleil max
- on peut donc prendre un plus petit PV mais les prix sont identiques
- en option, bme280, bmp280, ou combo bmp280-aht20 ( conseillé) , on ajoute la mesure de Température, Pression et Humidité relative

## Composants <a href="https://www.aliexpress.com/p/wish-manage/share.html?smbPageCode=wishlist-amp&spreadId=551cf74213882e701d1e87ed58798e6a94a8944ac3d0789751fec2fa515e353d">(liste d'achat)</a> :
- modules possibles :
  - arduino mkrfox1200
  - arduino pro mini + module :
    - LSM100A (CMS)
    - BRKWS01 (traversant)
- girouette, anémomètre, bras Misol
- module de charge Lithium TP4056
- coffret pour piles 18650 ( vous pouvez récupérer des piles dans une vieille batterie d'ordinateur portable )
- <a href="https://elec44.fr/eur-ohm/107264-eur-ohm-boite-de-derivation-etanche-ip55-couvercle-avec-vis-14-de-tour-155x110x80-mm-ref-50036-3663752011051.html">boitier étanche électrique</a>
- résistances diverses : 1k(x2), 10k, 100k, 390k, 560k
- condensateurs : 10nF (x2)
- diodes esr : 1N5818 (x4)
- interrupteur on/off, sur carte et/ou déporté étanche
- connecteur XH JST pour panneau solaire, batterie et inter
- connecteur rj11 pour CI
- si vous êtes dans une zone mal couverte, l'antenne livrée avec la carte arduino peut être insuffisante, acheter alors une 5dBi ou même 12dBi

## Assemblage carte électronique
- voir le schéma, il y a quelques composants à souder : résistances, condensateurs , diodes esr
- dessouder la led power sur la carte arduino
- vous pouvez acheter le circuit imprimé (vendus par 3) chez <a href="https://aisler.net/p/UPLBVEWD">https://aisler.net/p/UPLBVEWD</a>
- nouvelle carte ( arduino mkrfox1200 en fin de vie, option balise AIRMES ) : https://aisler.net/p/IGNIIGAD

- n'hésitez pas à me contacter, je peux peut-être vous en fournir au détail s'il m'en reste ( les petits composants aussi , je les ai achetés par 50 )

## Recommandations
- utiliser de la quincaillerie inox ou nylon
- vernir la carte pour prévenir l'oxydation
 
## Programmation
### pré-requis
- installer arduino IDE 2
- le cas échéant, via le gestionnaire de cartes, ajouter la mkrfox ( arduino SAMD boards )
- installer la bibliothèque mkrfox (arduino sigfox mkrfox1200 )
- bibliothèque arduino low power
### calibration
pour les balises misol, il est nécessaire de calibrer la girouette
- ouvrir le prog windsensor sigfox. suivre les instructions dans le fichier defs.h
### enregistrement de la balise sur le réseau openwindmap
- pendant la calibration, il faudra relever l'ID et le PAC à transmettre en privé à OpenWindMap sur le forum pour enregistrer la carte sur le réseau.

vous pouvez me contacter via gitlab ou le forum <a href="https://www.openwindmap.org/">openwindmap</a>, mon pseudo est dam74
